from setuptools import setup, find_packages

setup(
    author="R. C. Thomas",
    author_email="rcthomas@lbl.gov",
    description="Simplify starting a dask-mpi cluster at NERSC",
    entry_points = {
        "console_scripts": ["start-dask-mpi=nersc_dask.cli:cli"],
    },
    name="nersc-dask",
    packages=find_packages(exclude=["tests"]),
    version="0.0.2",
)
